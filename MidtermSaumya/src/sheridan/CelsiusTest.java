package sheridan;

/*
 * Name - Saumya Mehta
 * 
 * 
 * */

//991541351

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CelsiusTest {

//	@Test
//	void testFromFahrenheitException() {
//		fail("Test Case Fail");
//	}
	
	@Test
	void testMinimumRoundOffCelsius() {
		int res=Celsius.fromFahrenheit(34);
		System.out.println(res);
		assert(res==1);
	}

	
	@Test
	void testMaxRoundOffCelsius() {
		int res=Celsius.fromFahrenheit(35);
		System.out.println(res);
		assert(res==2);
	}
	
	
}
